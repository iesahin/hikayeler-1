# İlhami


date

:   2016-03-30 23:33:30 +0300

author

:   Emin Reşah

date

:   2012.02.21

Bu kitabın herhangi bir formatta dağıtımı, bütünlüğü korunduğu ve bu
metin yer aldığı sürece serbesttir.

Emacs ve LaTeX gibi özgür yazılımlar yardımıyla oluşturulmuştur.

© 2008-2012 Emin Reşah. Telif hakkı yazara aittir.

Takdim Tarihçesi
----------------

| 1. Takdim: 21 Aralık 2008 (*Hikayeler* adıyla)
| 2. Takdim: 21 Nisan 2011 (*Hikayeler* adıyla)
| 3. Takdim: 21 Şubat 2012

